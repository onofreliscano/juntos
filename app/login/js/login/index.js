function redirects(url){
    window.location.replace(url);
}

function fadeOut() {
    $("body").fadeOut();
    setTimeout(function() {
        redirects("../main/");
    }, 500)
}

function fadeIn() {
    $("body").fadeIn();
}

var working = false;
$('.login').on('submit', function(e) {
    e.preventDefault();
    if (working) return;
    working = true;
    var $this = $(this),
    $state = $this.find('button > .state');
    $this.addClass('loading');
    $state.html('Autenticando');

    if (document.getElementById('txtindicador').value=="LISCANOO") {
        setTimeout(function() {
            $this.addClass('ok');
            $state.html('Bienvenido(a): LISCANOO.');
            setTimeout(function() {
                //$state.html('Log in');
                //$this.removeClass('ok loading');
                //working = false;
            }, 2000);
        }, 2000);
        setTimeout(fadeOut, 3500);
    }
    else {
        setTimeout(function() {
            $this.addClass('wrong');
            $state.html('Datos inválidos.<br> Por favor corregir');
            setTimeout(function() {
                    $state.html('Aceptar');
                    $this.removeClass('wrong loading');
                    working = false;
                }, 4000);
            }, 3000);

  }
});
