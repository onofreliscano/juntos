<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Detail page demoxxx - jQuery.smoothState.js</title>

    <!-- Styles -->
    <!--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,800">-->
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/keyframes.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/pageTransitions.css">
  </head>
  <body>
    <!-- Detail page -->
<div class="m-scene" id="main">
  <div class="m-aside scene_element scene_element--fadein">
    ...
  </div>
  <div class="m-right-panel m-page scene_element scene_element--fadeinright">
    ...
  </div>
</div>
    <script src="https://code.jquery.com/jquery-2.2.1.min.js"></script>
    <script src="assets/js/jquery.smoothState.min.js"></script>
    <script src="assets/js/main.js"></script>
  </body>
</html>
