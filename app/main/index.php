<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>



		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title>Multi-Level Push Menu - Demo 1</title>
		<meta name="description" content="Multi-Level Push Menu: Off-screen navigation with multiple levels" />
		<meta name="keywords" content="multi-level, menu, navigation, off-canvas, off-screen, mobile, levels, nested, transform" />
		<meta name="author" content="Codrops" />
		<link rel="shortcut icon" href="../favicon.ico">
		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/demo.css" />
		<link rel="stylesheet" type="text/css" href="css/icons.css" />
		<link rel="stylesheet" type="text/css" href="css/component.css" />
		<script src="js/modernizr.custom.js"></script>

<script src="https://code.jquery.com/jquery-2.2.1.min.js"></script>
		


		<style>

			.headerGB {
  			top:0;

			width: 100%;
  
  			height: 50px;
  			background-color: white;
   			display: block;
  
}


			.headerPDVSA {
  			top:51;
  
 
  
  			width: 100%;
  
  			height: 50px;
  			background-color: red;
   			display: block;
  
}
		</style>

<script>
$(window).load(function(){
	$('body').hide().fadeIn(2500).delay(50000);
});


function toggleDiv(divId) {
	//alert ('Aca');
   $("#"+divId).hide();
   $("#myContent2").show();
}


</script>



	</head>
	<body>

		<div class="container" >
			<!-- Push Wrapper -->
			<div class="mp-pusher" id="mp-pusher">

				<!-- mp-menu -->
				<nav id="mp-menu" class="mp-menu">
					<div class="mp-level">
						<h2><strong>Opciones CVP</strong></h2>
						<ul>
							<li class="icon icon-arrow-left">
								<a class="icon icon-display" href="#">PCyG</a>
								<div class="mp-level">
									<h2>Servicios al personal</h2>
									<ul>
										<li class="icon icon-arrow-left">
											<a class="icon icon-phone" href="#">Avisos de cambio</a>
											<div class="mp-level">
												<h2>Avisos de cambio</h2>
												<ul>
													<li><a href="javascript:toggleDiv('myContent1');" onclick="myFunction()">Cargar/editar</a></li>
													<li><a href="#">Cargar PDF</a></li>
													<li><a href="#">consultas</a></li>
													<li><a href="#">Otros</a></li>
												</ul>
											</div>
										</li>
										<li class="icon icon-arrow-left">
											<a class="icon icon-tv" href="#">Indicadores</a>
											<div class="mp-level">
												<h2>Indicadores</h2>
												<ul>
													<li><a href="#"></a></li>
													<li><a href="#">Gigantic LED</a></li>
													<li><a href="#">Power Eater</a></li>
													<li><a href="#">3D Experience</a></li>
													<li><a href="#">Classic Comfort</a></li>
												</ul>
											</div>
										</li>
										<li class="icon icon-arrow-left">
											<a class="icon icon-camera" href="#">Cameras</a>
											<div class="mp-level">
												<h2>Cameras</h2>
												<ul>
													<li><a href="#">Smart Shot</a></li>
													<li><a href="#">Power Shooter</a></li>
													<li><a href="#">Easy Photo Maker</a></li>
													<li><a href="#">Super Pixel</a></li>
												</ul>
											</div>
										</li>
									</ul>
								</div>
							</li>
							<li class="icon icon-arrow-left">
								<a class="icon icon-news" href="#">Servicios al personal</a>
								<div class="mp-level">
									<h2 class="icon icon-news">Magazines</h2>
									<ul>
										<li><a href="#">National Geographic</a></li>
										<li><a href="#">Scientific American</a></li>
										<li><a href="#">The Spectator</a></li>
										<li><a href="#">The Rambler</a></li>
										<li><a href="#">Physics World</a></li>
										<li><a href="#">The New Scientist</a></li>
									</ul>
								</div>
							</li>
							<li class="icon icon-arrow-left">
								<a class="icon icon-shop" href="#">Formación</a>
								<div class="mp-level">
									<h2 class="icon icon-shop">Store</h2>
									<ul>
										<li class="icon icon-arrow-left">
											<a class="icon icon-t-shirt" href="#">Clothes</a>
											<div class="mp-level">
												<h2 class="icon icon-t-shirt">Clothes</h2>
												<ul>
													<li class="icon icon-arrow-left">
														<a class="icon icon-female" href="#">Women's Clothing</a>
														<div class="mp-level">
															<h2>Women's Clothing</h2>
															<ul>
																<li><a href="#">Tops</a></li>
																<li><a href="#">Dresses</a></li>
																<li><a href="#">Trousers</a></li>
																<li><a href="#">Shoes</a></li>
																<li><a href="#">Sale</a></li>
															</ul>
														</div>
													</li>
													<li class="icon icon-arrow-left">
														<a class="icon icon-male" href="#">Men's Clothing</a>
														<div class="mp-level">
															<h2>Men's Clothing</h2>
															<ul>
																<li><a href="#">Shirts</a></li>
																<li><a href="#">Trousers</a></li>
																<li><a href="#">Shoes</a></li>
																<li><a href="#">Sale</a></li>
															</ul>
														</div>
													</li>
												</ul>
											</div>
										</li>
										<li>
											<a class="icon icon-diamond" href="#">Jewelry</a>
										</li>
										<li>
											<a class="icon icon-music" href="#">Music</a>
										</li>
										<li>
											<a class="icon icon-food" href="#">Grocery</a>
										</li>
									</ul>
								</div>
							</li>
							<li><a class="icon icon-photo" href="#">Captación</a></li>
							<li><a class="icon icon-wallet" href="#">Otras gerencias</a></li>
						</ul>
					</div>
				</nav>
				<!-- /mp-menu -->
				<div class="headerGB"> Header Gobierno Bolivariano</div>
				<div class="headerPDVSA"> Header PDVSA</div>
				<p><a href="#" id="trigger" class="menu-trigger"> </a></p>

				<div  class="scroller"><!-- this is for emulating position fixed of the nav -->
					<div  class="scroller-inner" id="myContent1" style="display:block">
						<?php include 'content1.php';?>

					</div><!-- /scroller-inner -->


					<div  class="scroller-inner" id="myContent2" style="display:none">
						<?php include 'content2.php';?>

					</div><!-- /scroller-inner -->


					<div  class="scroller-inner" id="myContent3" style="display:none">
						<?php include 'content3.php';?>

					</div><!-- /scroller-inner -->
					
				</div><!-- /scroller -->

					
				</div><!-- /scroller -->
			</div><!-- /pusher -->
		</div><!-- /container -->





		<script src="js/classie.js"></script>
		<script src="js/mlpushmenu.js"></script>
		<script>
			new mlPushMenu( document.getElementById( 'mp-menu' ), document.getElementById( 'trigger' ) );
		</script>





	</body>
</html>